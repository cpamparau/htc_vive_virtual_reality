﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using Valve.VR;
using WebSocketSharp;

public class VoiceRecognition : MonoBehaviour {

	[SerializeField]
	private string[] m_Keywords;

	WebSocket ws;

	private KeywordRecognizer m_Recognizer;

	public GameObject obj;

	public static bool canStop = false;

	private bool isVibrating = true;

	public SteamVR_Action_Vibration haptic;

	public SteamVR_Action_Boolean trackpadAction;
	
	// Use this for initialization
	public void Start () {
		m_Keywords = new string[1];
		m_Keywords[0] = "Table";
		//m_Keywords[1] = "Stop";
		m_Recognizer = new KeywordRecognizer(m_Keywords);
		m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
		m_Recognizer.Start();
		ws = new WebSocket("ws://192.168.10.8:8081");
		ws.Connect();
		ws.Send("#device");
		ws.OnMessage += (sender, e) =>
		{
			byte[] bytes = e.RawData;
			Debug.Log(bytes);
			string sir = System.Text.Encoding.UTF8.GetString(bytes);
			Debug.Log("Message Received from " + ((WebSocket)sender).Url + ", Data : " + sir);
			if (sir.Contains("150"))
            {
				Pulse((float)0.15, 150, 1, SteamVR_Input_Sources.RightHand);
				Debug.Log("Am activat vibratia");
				
			}
			//string[] siruri = sir.Split(' ');
		};
	}

	private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
		if (args.text == m_Keywords[0])
        {
			Debug.Log("Am recunoscut Table, pornim vibratia");
			//Pulse(2, 5, 85, SteamVR_Input_Sources.RightHand);
		}
	}

	public void Update()
	{
/*		var anim = obj.GetComponent<Animation>();
		if (canStop)
		{
			anim.Stop();
		}
		else
		{
			anim.Play();
		}*/

	}
		

	private void Pulse(float duration, float frequency, float amplitude, SteamVR_Input_Sources source)
    {
		haptic.Execute(0, duration, frequency, amplitude, source);
		print("Pulse " + source.ToString()); 
    }
}
